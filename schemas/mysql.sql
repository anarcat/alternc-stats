CREATE TABLE IF NOT EXISTS `stat_http` (
  `uid` int(10) unsigned NOT NULL default '0',
  `day` date NOT NULL default '0000-00-00',
  `domain` varchar(255) NOT NULL default '',
  `hit` int(10) unsigned NOT NULL default '0',
  `size` int(10) unsigned NOT NULL default '0',
  PRIMARY KEY  (`day`,`domain`),
  KEY `uid` (`uid`),
  KEY `day` (`day`)
) TYPE=MyISAM COMMENT='Hits and bandwith used by domains';
