<?php
/*
 $Id: m_stats_web.php 1401 2005-05-02 01:40:11Z anarcat $
 ----------------------------------------------------------------------
 LICENSE

 This program is free software; you can redistribute it and/or
 modify it under the terms of the GNU General Public License (GPL)
 as published by the Free Software Foundation; either version 2
 of the License, or (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 To read the license please visit http://www.gnu.org/copyleft/gpl.html
 ----------------------------------------------------------------------
 Original Author of file: Benjamin Sonntag
 Purpose of file: Manage bandwidth usage statistics
 ----------------------------------------------------------------------
*/
/**
* Classe de gestion des statistiques d'acc�s web.
*
* Cette classe permet de g�rer les Comptes FTP d'un membre h�berg�.<br />
* Copyleft {@link http://alternc.net/ AlternC Team}
*
* @copyright    AlternC-Team 2002-11-01 http://alternc.net/
*
*/
class m_stats_web {

  /* ----------------------------------------------------------------- */
  /**
   * Constructeur
   */
  function m_stats_web() {
  }

  /* ----------------------------------------------------------------- */
  /**
   * Quota name
   */
  function alternc_quota_names() {
    return "bw_web";
  }

  /* ----------------------------------------------------------------- */
  /** 
   * Returns the used quota for the $name service for the current user.
   *
   * This is the bytecount in this month
   * @param $name string name of the quota 
   * @return integer the number of service used or false if an error occured
   * @access private
   */
  function alternc_get_quota($name) {
    global $db,$err,$cuid;
    if ($name=="bw_web") {
      $err->log("bw_web","getquota");
      $db->query("
SELECT SUM( size ) as cnt
FROM `stat_http`
WHERE `day` < CONCAT( date_format( DATE_ADD( NOW( ) , INTERVAL 1
MONTH ) , '%Y-%m' ) , '-01' )
AND `day` >= CONCAT( date_format( now( ) , '%Y-%m' ) , '-01' )
AND `uid`= '$cuid'
");
      $db->next_record();
      $cnt = $db->f("cnt");
      if (empty($cnt)) {
        $cnt = 0;
      }
      return $cnt;
    } else return false;
  }

} /* Class m_ftp */

?>
