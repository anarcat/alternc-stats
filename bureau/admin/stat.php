<?php

require_once("../class/config.php");
require_once("stat_functions.php");

// Choix niveau 1 : sur quoi va porter la statistique / etiquettes : 

include("head.php");

echo "<h1>Moteur de statistiques de consommation de bande passante</h1>";

if ($error) echo "<div class=\"error\">$error</div>";
?>
<p>
<a href="stat_list.php">Nouveau tableau / graphique / rapport de bande passante ...</a>
</p>
<p>
&nbsp; Si vous avez sauv� des requ�tes pr�programm�es, vous pouvez y acc�der ci-dessous. <br />
&nbsp; Pour cela, cliquez sur "Modifier" pour voir les param�tres d'une requ�te, ou "Executer" pour obtenir le r�sultat 
tout de suite (document pdf, excel, calcul de resultat...)
</p>

<?php

$r=$db->query("SELECT * FROM bw_stats WHERE uid='$cuid' ORDER BY ts DESC");

if (!$db->next_record()) {
  echo "<h2>Vous n'avez enregistr� aucune requ�te</h2>";
} else {
?>
<table cellspacing="0" cellpadding="4">
<tr><th colspan="3">Action</th><th>Nom</th><th>Date</th></tr>
<?php
$col=1;
do {
  $c=$db->Record;
  $col=3-$col;
  echo "<tr class=\"lst$col\">";
  echo "<td><a href=\"stat_load.php?id=$c[id]\">Modifier</a></td>";
  echo "<td><a href=\"stat_exec.php?id=$c[id]\">Executer</a></td>";
  echo "<td><a href=\"stat_del.php?id=$c[id]\">Effacer</a></td>";
  echo "<td>$c[name]</td><td>".date_ts2fr($c[ts])."</td>";
  echo "</tr>";
} while ($db->next_record());
?>
</table>

<?php

}

?>
</body>
</html>
