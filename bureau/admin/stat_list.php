<?php

require_once("../class/config.php");
require_once("stat_functions.php");


if ($back) {
  header("Location: ".$ABSWEB."stat.php");
}

$qname=trim($qname);

if ($compute) {
	if (($r=="a" || $r=="b") && !count($ra)) {
		$error="Il faut au moins choisir un champ ! ";
	} else {

	  // Calcul maintenant : on fait faire le calcul ailleurs :) 
	  require_once("stat_dolist.php");
	}
}
if ($saveit && !$qname) {
  $error=_("Vous devez sp�cifier un nom de requ�te pour pouvoir la sauver !");
}
if ($saveit && $qname) {
  // Sauvegarde de la requete type : 
  stripslashes_rec($_POST);
  unset($_POST[qname]);
  unset($_POST[saveit]);

  $db->query("INSERT INTO bw_stats SET name='$qname', uid='$cuid', params='".addslashes(serialize($_POST))."';");
  $error=_("Requ�te sauvegard�e avec succ�s");
}


require_once("head.php");


?>
<h1><?php __("Moteur de statistiques de consommation de bande passante"); ?></h1>
<form method="post" action="stat_list.php" id="st" name="st">
<?php
if ($error) {
  echo "<h6>$error</h6>";
}
?>
<p>
Vous souhaitez dresser un listing, choisissez tout d'abord sur quoi portera cette liste, 
<br />puis utilisez les restrictions 
<br />enfin, choisissez le format de sortie (tableau, graphique, csv ...)
</p>
<p>

<?php
	// on veut dresser une liste.
$apa=array(
	"z"=>"-- Choisir --",
	"a"=>"La bande passante utilis�e par un site web",
        "b"=>"Le nombre de hits d'un site web",
);
?>
Ma requ�te porte sur : <br />
<select class="inl" name="pa" onchange="document.forms.st.submit()">
	<?php array_sel($pa,$apa); ?>
</select><br />

<!-- ########################################### -->
<?php 

// RESTRICTIONS : 

if (
   $pa && ($pa=="a" || $pa=="b")
   ) { // AJOUTER LES AUTRES CAS POSSIBLES ICI.
$aq=array(
        "z"=>"-- Choisir --",
	"a"=>"Pour les domaines ci-dessous ...",
	"b"=>"La somme les domaines ci-dessous ...",
);


// Selon les droits, on restreint automatiquement : 
if ($mem->checkright()) {
  $aq[c]="Pour les comptes AlternC ci-dessous ...";
  $aq[d]="La somme des comptes AlternC ci-dessous ...";
}

if (!$q) $q="z";
?>
Domaine de production de statistiques : <br />
<select class="inl" name="q" onchange="document.forms.st.submit()">
	<?php array_sel($q,$aq); ?>
</select><br />


<?php 

if ($q=="a" || $q=="b") {
  $aqa=array();
  // Choix parmi les domaines accessibles � l'utilisateur courant : 
  if ($mem->checkright()) {
    // Liste des domaines pointant en local pour MOI uniquement.
    $db->query("SELECT sub, domaine, login FROM sub_domaines s, membres m WHERE m.uid=s.compte AND m.creator='$cuid' ORDER BY sub,domaine;");
    while ($db->next_record()) {
       $aqa[$db->f("sub").(($db->f("sub"))?".":"").$db->f("domaine")]=$db->f("sub").(($db->f("sub"))?".":"").$db->f("domaine")." (compte ".$db->f("login").")";
    }
  } else {
    // Liste des domaines de tous les comptes que JE g�re, pointant en local.
    $db->query("SELECT sub,domaine FROM sub_domaines WHERE compte='$cuid' AND type=0 ORDER BY sub,domaine;");
    while ($db->next_record()) {
      $aqa[$db->f("sub").(($db->f("sub"))?".":"").$db->f("domaine")]=$db->f("sub").(($db->f("sub"))?".":"").$db->f("domaine");
    }
  }

if (count($aqa)<8) $hh=count($aqa); else $hh=8;

// On fait choisir le(s) domaines voulus : 
if (!$qa) $qa=array();
?>
Choisissez les domaines concern�s : <br />
<select class="inl" name="qa[]" multiple="multiple" size="<?php echo $hh; ?>">
	<?php array_multiple($qa,$aqa); ?>
</select><br />

<?php
} // Par domaines 
if ($q=="c" || $q=="d") {
  $aqc=array();
  $db->query("SELECT login FROM membres WHERE creator='$cuid' ORDER BY login;");
  while ($db->next_record()) {
    $aqc[$db->f("login")]=$db->f("login");
  }

if (count($aqc)<8) $hh=count($aqc); else $hh=8;

if (!$qc) $qc=array();
?>
Choisissez les comptes AlternC concern�s : <br />
<select class="inl" name="qc[]" multiple="multiple" size="<?php echo $hh; ?>">
        <?php array_multiple($qc,$aqc); ?>
</select><br />

<?php

} // Par compte AlternC

} // Q = A or B. (bw http ou hit http)
?>


<!-- ########################################### -->


<?php
if ($pa!="z") {
  // Choix de la date ou de la p�riode couverte.
  $ar=array(
    "z" => "-- Choix --",
    "a" => "Pour une date en particulier",
    "b" => "Entre 2 dates (tous les jours)",
    "c" => "Entre 2 dates (cumul)",
    "d" => "Entre 2 mois (tous les mois)",
    "e" => "Les 7 derniers jours",
    );
  if (!$r) $r="z";
?>
Choisissez la p�riode de calcul / d'affichage : <br />
<select class="inl" name="r" onchange="document.forms.st.submit()">
        <?php array_sel($r,$ar); ?>
</select><br />

<?php

if ($r=="a") {
  // choix d'une date particuli�re : 
?>
Entrez la date de statistique voulue (jj/mm/aaaa) : <br />
<input class="int" type="text" size="10" maxlength="10" name="ra" value="<?php echo $ra; ?>" />
<br />
<?php

} // ra

if ($r=="b" || $r=="c") {
  // choix de 2 dates particuli�res :
?>
Entrez l'interval de dates voulu (jj/mm/aaaa) : <br />
Du <input class="int" type="text" size="10" maxlength="10" name="rba" value="<?php echo $rba; ?>" /> au <input class="int" type="text" size="10" maxlength="10" name="rbb" value="<?php echo $rbb; ?>" />
<br />
<?php

} // rb rc

if ($r=="d") {
  // choix de 2 mois particuliers :
?>
Entrez l'interval des mois voulus (bornes incluses) (mm/aaaa) : <br />
Du <input class="int" type="text" size="7" maxlength="7" name="rda" value="<?php echo $rda; ?>" /> au <input class="int" type="text" size="7" maxlength="7" name="rdb" value="<?php echo $rdb; ?>" />
<br />
<?php

} // rd

} // Dates (r) 


?>

<!-- ########################################### -->


<?php

if (  $pa!="z" && 
      (($pa=="a" || $pa=="b") && $q!="z"
      ) &&
      $r!="z"  
   ) {

// FORMAT DE SORTIE : 
if (!$z) $z="z";

$az=array(
	"z"=>"-- Choisir --",
	"a"=>"Tableau Web",
	"b"=>"Format Excel (csv)",
	"c"=>"Tableau Web avec graphique",
);
?>
<hr />
Format de sortie : <br />
<select class="inl" name="z" onchange="document.forms.st.submit()">
	<?php array_sel($z,$az); ?>
</select><br />
<?php

if ($z!="z") {
   // Ok, on affiche le bouton ou la sauvegarde...
?>

<p>
Vous avez rempli tous les crit�res, vous pouvez maintenant proc�der au calcul ou sauver cette requ�te pour plus tard
</p>
<p>
<input class="inb" type="submit" name="compute" value="- = - = - Proc�der au calcul - = - = -" />
<br />
<input class="inb" type="submit" name="saveit" value="Sauver cette requ�te (entrez un nom ci-contre)" />
 Nom de la requ�te : <input class="int" type="text" name="qname" value="<?php echo fl(stripslashes($qname)); ?>" size="30" maxlength="255" /> 
</p>


<?php


}

} // Format de sortie

?>

<p>
<input class="inb" type="submit" name="back" value="Annuler, Retour" style="margin: 3px" />
</p>

<?php

exit();





















/*

?>
<select name="q" onchange="document.forms.st.submit()">
        <?php array_sel($q,$aaq); ?>
</select><br />
<?php 
if ($q=="a") {
unset($qb);
if (!$qa)  $qa=$ctid;
?>
<select name="qa" onchange="document.forms.st.submit()">
	<?php select_territoire($qa); ?>
</select><br />

<?php } // Choix d'un territoire 
?>

<?php 
if ($q=="b") {
unset($qa);
 if (!$qb) $qb=$creg;
 echo "<b>R�gion ".get_region($qb)."</b><input type=\"hidden\" name=\"qb\" value=\"$creg\" /><br />";

 } // Choix d'une region

} elseif ($right==3 || $right==4) { // restriction au territoire :)
if (!$q) $q="a";
?>
Restriction Geographique : <br />
<b><?php echo get_territoire($ctid); ?></b>
<input type="hidden" name="q" value="a" />
<input type="hidden" name="qa" value="<?php echo $ctid; ?>" />
<br />
<?php
$q="a";
$qa=$ctid;
}  elseif ($right==1 || $right==5 || $right==6) { // national :)
if (!$q) $q="z";
?>
Restriction Geographique : <br />
<select name="q" onchange="document.forms.st.submit()">
	<?php array_sel($q,$aq); ?>
</select><br />

<?php 

if ($q=="a") {
unset($qb);
?>
<select name="qa" onchange="document.forms.st.submit()">
<option value="">-- Choix d'un territoire -- </option>
	<?php select_territoire($qa,-1); ?>
</select><br />

<?php } // Choix d'un territoire 
?>

<?php 
if ($q=="b") {
unset($qa);
?>
<select name="qb" onchange="document.forms.st.submit()">
<option value="">-- Choix d'une r�gion -- </option>
	<?php select_region($qb); ?>
</select><br />

<?php } // Choix d'une region

if ($q!="a" && $q!="b") {
  unset($qa); unset($qb);
}
?>


<?php } // Zone geo (q)
}
else {
unset($qa); unset($qb);
}
?>

<!-- ########################################### -->
<?php 
if ($pa && $pa!="e" && $pa!="f" && $pa!="z") {
// RESTRICTIONS PROJET : 
$ao=array(
    "z"=>"Pas de crit�re de Projets / Types de projet",
    "b"=>"Choix des projets ci-dessous",
    "c"=>"Choix d'un type de projet ci-dessous",
);
if (!$o) $o="z";
?>
Restriction par Projet / Type de projet : <br />
<select name="o" onchange="document.forms.st.submit()">
<?php array_sel($o,$ao); ?>
</select><br />

<?php
if ($o=="b") {

// on dresse la liste des projets : 
if ($qa) { // d'un territoire 
$re=mq("SELECT id,concat(nom,' - ',ville) as n FROM projets WHERE tid='$qa' ORDER BY nom ;");
}
elseif ($qb) { // d'une region
$re=mq("SELECT p.id, concat(p.nom,' - ',t.territoire,' - ',p.ville) AS n FROM projets p INNER JOIN territoires t ON t.id=p.tid WHERE t.region='$qb' ORDER BY p.tid,p.nom;");
}
else { // tous les projets
$re=mq("SELECT p.id, concat(p.nom,' - ',t.territoire,' - ',p.ville) AS n FROM projets p INNER JOIN territoires t ON t.id=p.tid ORDER BY p.tid,p.nom;");
}
$hh=8;
if (mysql_num_rows($re)<8) $hh=mysql_num_rows($re);
while ($c=mysql_fetch_array($re)) {
  $aob["p".$c[id]]=$c[n];
}
if (!is_array($ob)) {
$ob=array();
}
?>
Choisir le(s) projet(s) s�lectionn�s : <br />
<select name="ob[]" multiple="multiple" size="<?php echo $hh; ?>">
	<?php array_multiple($ob,$aob); ?>
</select><br />

<?php } // Choix d'un projet

if ($o=="c") { 
// on dresse la liste des types de projet.
$re=mq("SELECT id,data FROM type_actions ORDER BY data");
while ($c=mysql_fetch_array($re)) {
$aoc["p".$c[id]]=$c[data];
}
?>
<select name="oc" onchange="document.forms.st.submit()">
<?php array_sel($oc,$aoc); ?>
</select><br />
<?php
}
}
?>







<!-- ########################################### -->
<?php 
if ($pa && $pa=="b") {
// RESTRICTIONS PAR UNIVERSITE
$as=array(
    "z"=>"Pas de restriction par universit�",
    "b"=>"Uniquement les �tudiants de l'universit� ci-dessous",
);
if (!$s) $s="z";
?>
Restriction par Universit� : <br />
<select name="s" onchange="document.forms.st.submit()">
<?php array_sel($s,$as); ?>
</select><br />

<?php
if ($s=="b") {

// on dresse la liste des universit�s 
if ($qa) { // d'un territoire 
	// on capte la r�gion du territoire : 
	$trr=get_terr_reg($qa);
  $re=mq("SELECT p.id, concat(p.nom,' - ',t.territoire,' - ',p.ville) AS n FROM univ p INNER JOIN territoires t ON t.id=p.tid WHERE t.region='$trr' ORDER BY p.tid,p.nom;");
}
elseif ($qb) { // d'une region
  $re=mq("SELECT p.id, concat(p.nom,' - ',t.territoire,' - ',p.ville) AS n FROM univ p INNER JOIN territoires t ON t.id=p.tid WHERE t.region='$qb' ORDER BY p.tid,p.nom;");
}
else { // tous les projets
  $re=mq("SELECT p.id, concat(p.nom,' - ',t.territoire,' - ',p.ville) AS n FROM univ p INNER JOIN territoires t ON t.id=p.tid ORDER BY p.tid,p.nom;");
}
$hh=8;
if (mysql_num_rows($re)<8) $hh=mysql_num_rows($re);
while ($c=mysql_fetch_array($re)) {
  $asb["p".$c[id]]=$c[n];
}

?>
Choisir l'universit� : <br />
<select name="sb" >
	<?php array_sel($sb,$asb); ?>
</select><br />

<?php } // Choix d'une universit�

}
?>



<!-- ########################################### -->
<?php

// Crit�res sp�cifiques aux enfants
if ($pa=='d') {
?>
Crit�res sp�cifiques aux enfants : <br />

Classe de l'enfant <select name="ta">
  <option value="0">Peu importe</option>
	<?php select_classe($ta); ?>
</select> <br />

Sexe de l'enfant : <select name="tb">
  <option value="0">Peu importe</option>
<option value="1"<?php if ($tb==1)  echo " selected=\"selected\""; ?>>Gar�on</option>
<option value="2"<?php if ($tb==2)  echo " selected=\"selected\""; ?>>Fille</option>
</select> <br />

<?php
// RESTRICTIONS PAR ETABLISSEMENT
$atc=array(
    "z"=>"Pas de restriction par �tablissement",
    "b"=>"Uniquement les enfants de l'�tablissement ci-dessous",
    "c"=>"Uniquement les enfants des �tablissements du type ci-dessous",
);
if (!$tc) $tc="z";
?>
Restriction par Etablissement : <br />
<select name="tc" onchange="document.forms.st.submit()">
<?php array_sel($tc,$atc); ?>
</select><br />

<?php
if ($tc=="b") {

// on dresse la liste des etablissements
if ($qa) { // d'un territoire 
  $re=mq("SELECT p.id, concat(p.nom,' - ',p.ville) AS n FROM ets p WHERE p.tid='$qa' ORDER BY p.nom;");
}
elseif ($qb) { // d'une region
  $re=mq("SELECT p.id, concat(p.nom,' - ',t.territoire,' - ',p.ville) AS n FROM ets p INNER JOIN territoires t ON t.id=p.tid WHERE t.region='$qb' ORDER BY p.tid,p.nom;");
}
else { // tous les �tablissements
  $re=mq("SELECT p.id, concat(p.nom,' - ',t.territoire,' - ',p.ville) AS n FROM ets p INNER JOIN territoires t ON t.id=p.tid ORDER BY p.tid,p.nom;");
}
$hh=8;
if (mysql_num_rows($re)<8) $hh=mysql_num_rows($re);
while ($c=mysql_fetch_array($re)) {
  $atcb["p".$c[id]]=$c[n];
}

?>
Choisir l'�tablissement : <br />
<select name="tcb" >
	<?php array_sel($tcb,$atcb); ?>
</select><br />

<?php } // Choix d'un etablissement 
if ($tc=="c") { // Type d'�tablissement 

?>
Choisir le type d'�tablissement : <br />
<select name="tcc" >
	<?php select_typeets($tcc); ?>
</select><br />

<?php } // Choix d'un type d'etablissement 
 ?>



<?php } // Enfant 
 ?>

<!-- ########################################### -->

<?php if ($pa=="e") { // ETABLISSEMENTS 

// RESTRICTIONS PAR ETABLISSEMENT
$ate=array(
    "z"=>"Tous les types d'�tablissement",
    "a"=>"Uniquement les �tablissements du type ci-dessous",
);
if (!$te) $te="z";
?>
Restriction : <br />
<select name="te" onchange="document.forms.st.submit()">
<?php array_sel($te,$ate); ?>
</select><br />

<?php
if ($te=="a") {

?>
Choisir le type d'�tablissement : <br />
<select name="tea" >
	<?php select_typeets($tea); ?>
</select><br />

<?php } // Choix d'un type d'etablissement 


} // ETABLISSEMENTS 

?>

<!-- ########################################### -->

<?php if ($pa=="f") { // UNIVERSITE 

// RESTRICTIONS PAR UNIVERSITE
$atf=array(
    "z"=>"Tous les types d'universit�",
    "a"=>"Uniquement les universit�s du type ci-dessous",
);
if (!$tf) $tf="z";
?>
Restriction : <br />
<select name="tf" onchange="document.forms.st.submit()">
<?php array_sel($tf,$atf); ?>
</select><br />

<?php
if ($tf=="a") {

?>
Choisir le type d'universite : <br />
<select name="tfa" >
	<?php select_typeuniv($tfa); ?>
</select><br />

<?php } // Choix d'un type d'universite 


} // UNIVERSITES 

?>


<!-- ########################################### -->
<?php

// Crit�res sp�cifiques aux �tudiants
if ($pa=='b') {
?>
Crit�res sp�cifiques aux �tudiants : <br />

Fili�re de l'�tudiant <select name="nr">
  <option value="0">Peu importe</option>
	<?php select_filiere($nr); ?>
</select> <br />

Tranche d'age ? <select name="nl" onchange="document.forms.st.submit()">
  <option value="0">Peu importe</option>
  <option value="1"<?php if ($nl==1)  echo " selected=\"selected\""; ?>>Antre les �ges ci-dessous</option>
</select> <br />
<?php
if ($nl==1) {
?>
Entre <select name="nla">
<?php 
	for($i=10;$i<=30;$i++) { 
		echo "<option";
		if ($nla==$i) echo " selected=\"selected\"";
		echo ">$i</option>";
	}
?>
</select> ans (inclus) et <select name="nlb">
<?php 
	for($i=10;$i<=40;$i++) { 
		echo "<option";
		if ($nlb==$i) echo " selected=\"selected\"";
		echo ">$i</option>";
	}
?>
</select> ans (exclus) <br />
<?php
}
?>

Etudiant Adh�rent ? <select name="na">
  <option value="0">Peu importe</option>
  <option value="1"<?php if ($na==1)  echo " selected=\"selected\""; ?>>Oui</option>
  <option value="2"<?php if ($na==2)  echo " selected=\"selected\""; ?>>Non</option>
</select> <br />
Etudiant R�f�rent ? <select name="nb">
  <option value="0">Peu importe</option>
  <option value="1"<?php if ($nb==1)  echo " selected=\"selected\""; ?>>Oui</option>
  <option value="2"<?php if ($nb==2)  echo " selected=\"selected\""; ?>>Non</option>
</select> <br />
De niveau d'�tude <select name="nd" onchange="document.forms.st.submit()">
  <option value="0">Peu importe</option>
  <option value="1"<?php if ($nd==1)  echo " selected=\"selected\""; ?>> Choisir les ann�es post-bac ... </option>
</select>
<?php 

if (!is_array($ne)) $ne=array(); 
if ($nd==1) { ?>
<br />

<table><tr><td>
    <select name="ne[]" size="9" multiple=\"multiple\">
<?php 
$nea=array();
for($a=0;$a<=8;$a++) $nea[$a]="Bac +$a";
 array_multiple($ne,$nea); 
?>
    </select>
</td>
</td><td style="padding-left: 40px">
Vous pouvez effectuer une s�lection multiple dans cette liste<br />
Sur PC utilisez la touche &lt;Control&gt; et cliquez sur les �lements<br />
pour les s�lectionner / d�s�lectionner. <br />
Sur Mac, utilisez la touche &lt;Pomme&gt;
</td></tr></table>

<?php } ?>
<br />
Statut de l'�tudiant : <select name="nf">
<option value="0">Peu importe</option>
<?php
  for ($a=1;$a<=count($a_sta_etu);$a++) {
echo "<option value=\"$a\"";
if ($nf==$a) echo " selected=\"selected\"";
echo ">".$a_sta_etu[$a]."</option>";
}
?>
</select> <br />

L'�tudiant a un v�hicule : <select name="ng">
  <option value="0">Peu importe</option>
  <option value="1"<?php if ($ng==1)  echo " selected=\"selected\""; ?>>Oui</option>
  <option value="2"<?php if ($ng==2)  echo " selected=\"selected\""; ?>>Non</option>
</select> <br />

Sexe de l'�tudiant : <select name="nh">
  <option value="0">Peu importe</option>
<option value="1"<?php if ($nh==1)  echo " selected=\"selected\""; ?>>Gar�on</option>
<option value="2"<?php if ($nh==2)  echo " selected=\"selected\""; ?>>Fille</option>
</select> <br />

Mode d'arriv�e de l'�tudiant : <select name="ni">
  <option value="0">Peu importe</option>
<?php
select_modearr($ni);
?>
</select> <br />

L'Etudiant lit son mail r�guli�rement : <select name="nj">
  <option value="0">Peu importe</option>
  <option value="1"<?php if ($nj==1)  echo " selected=\"selected\""; ?>>Oui</option>
  <option value="2"<?php if ($nj==2)  echo " selected=\"selected\""; ?>>Non</option>
</select> <br />



<?php } ?>

<?php 
// sp�cifique �tudiant / salari�s
if ($pa=='a' || $pa=='b' || $pa=='c' || $pa=='d') { 

if ($pa=='a') echo "Salari�s";
if ($pa=='b') echo "Etudiants";
if ($pa=='c') echo "Salari�s ou Etudiants";
if ($pa=='d') echo "Enfants";
?>
 dont le NOM contient <input type="text" name="nc" value="<?php echo fl($nc); ?>" /> <br />
	<?php if ($pa!='d') { ?>
		dont l'ADRESSE 2 contient <input type="text" name="ncb" value="<?php echo fl($ncb); ?>" /> <br />
	<?php } ?>
<?php } ?>

<!-- ########################################### -->

<?php 

// FORMAT DE SORTIE : 

$ar=array(
	"z"=>"-- Choisir --",
	"a"=>"Liste Web",
	"b"=>"Format Excel (csv)",
	"c"=>"Etiquettes d'adresses en pdf (Avery dp245 8/3)",
	"d"=>"Envoyer un email aux personnes de cette liste",
	"e"=>"CALCUL sorti en liste web", 
);
?>
<hr />
Format de sortie : <br />
<select name="r" onchange="document.forms.st.submit()">
	<?php array_sel($r,$ar); ?>
</select><br />

<?php

if ($r=="a" || $r=="b") {
  // Proposer le choix des champs.
switch ($pa) {
  case "a": // salari�s
  $ara=array("nom"=>"Nom","prenom"=>"Pr�nom",
    "email"=>"Email","adresse"=>"Adresse postale",
    "zip"=>"Code Postal","ville"=>"Ville",
    "tel"=>"Num�ro de t�l�phone","portable"=>"T�l�phone Portable",
    "birth"=>"Date de naissance","struct"=>"Structure associ�e",
    "tid"=>"Territoire de rattachement","rid"=>"R�gion de rattachement",
    "droit"=>"Droit d'acc�s (num�ro)","login"=>"Nom d'utilisateur"
    );
  break;
  case "b": // etudiants
  $ara=array("nom"=>"Nom","prenom"=>"Pr�nom",
    "email"=>"Email","adresse"=>"Adresse postale",
    "zip"=>"Code Postal","ville"=>"Ville",
    "tel"=>"Num�ro de t�l�phone","portable"=>"T�l�phone Portable",
    "birth"=>"Date de naissance",
    "tid"=>"Territoire de rattachement","rid"=>"R�gion de rattachement",
    "login"=>"Nom d'utilisateur","adherent"=>"Est-ce un adh�rent",
    "referent"=>"Est-ce un r�f�rent",
    "voiture"=>"A-t-il une voiture", "readmail"=>"Lit-il son mail",
    "datearr"=>"Date d'arriv�e � l'afev",
    "niveau"=>"Niveau d'�tude","etusta"=>"Etat de l'�tudiant (num�ro)",
    "modearr"=>"Mode d'arriv�e","filiere"=>"Fili�re �tudiante",
    "solidaire"=>"A-t-il d�j� particip� en solidarit�","soldetail"=>"D�tail de sa participation solidaire",
    "univ"=>"Universit� de rattachement (nom)","univtype"=>"Type de l'universit� de rattachement",
    "univville"=>"Ville de l'universit� de rattachement","sexe"=>"Sexe",
    "adresse2"=>"Adresse postale secondaire","zip2"=>"Code postal secondaire",
    "ville2"=>"Ville secondaire",
    );
  break;
  case "c": // les deux
  $ara=array("nom"=>"Nom","prenom"=>"Pr�nom",
    "email"=>"Email","adresse"=>"Adresse postale",
    "zip"=>"Code Postal","ville"=>"Ville",
    "tel"=>"Num�ro de t�l�phone","portable"=>"T�l�phone Portable",
    "birth"=>"Date de naissance",
    "tid"=>"Territoire de rattachement","rid"=>"R�gion de rattachement",
    "droit"=>"Droit d'acc�s (num�ro)","login"=>"Nom d'utilisateur",
    "type"=>"Type : etudiant ou salari�",
    );
  break;
  case "d": // enfants
  $ara=array("nom"=>"Nom","prenom"=>"Pr�nom", "adresse"=>"Adresse postale",
    "zip"=>"Code Postal","ville"=>"Ville",
    "tel"=>"Num�ro de t�l�phone","birth"=>"Date de naissance",
    "tid"=>"Territoire de rattachement","rid"=>"R�gion de rattachement",
    "eid"=>"Etablissement de rattachement (nom)","eidtype"=>"Type d'�tablissement de rattachement",
    "projet" => "Projet de rattachmenet", "sexe"=>"Sexe", "classe"=>"Classe"
    );
  break;
  case "e": // etablissements
  $ara=array("nom"=>"Nom","adresse"=>"Adresse postale",
    "zip"=>"Code Postal","ville"=>"Ville",
    "tel"=>"Num�ro de t�l�phone","email"=>"Email",
    "etype"=>"Type d'�tablissement",
    "tid"=>"Territoire de rattachement","rid"=>"R�gion de rattachement",
    );
  break;
  case "f": // universit�s
  $ara=array("nom"=>"Nom","adresse"=>"Adresse postale",
    "zip"=>"Code Postal","ville"=>"Ville",
    "tel"=>"Num�ro de t�l�phone","email"=>"Email",
    "etype"=>"Type d'universite",
    "tid"=>"Territoire de rattachement","rid"=>"R�gion de rattachement",
    );

  break;
  }
if (is_array($ara)) {
if (!is_array($ra)) {
// Par d�faut, on s�lectionne tous les champs : 
//$ra=array_keys($ara);
// ou aucun champ ...
$ra=array();
}
?>

<table><tr><td>
Choisissez les champs � afficher sur la page r�sultat : <br />
<select name="ra[]" size="14" multiple="multiple" >
<?php array_multiple($ra,$ara); ?>
</select>
</td><td style="padding-left: 40px">
Vous pouvez effectuer une s�lection multiple dans cette liste<br />
Sur PC utilisez la touche &lt;Control&gt; et cliquez sur les �lements<br />
pour les s�lectionner / d�s�lectionner. <br />
Sur Mac, utilisez la touche &lt;Pomme&gt;
</td></tr></table>
<br />

<?php
} }

if (!$rre) $rre="z";

if ($r=="e") {
  // Proposer le choix des regroupements possibles : 
  $are=array(
	"z"=>"--- Choix d'un champ de regroupement ---",
	"a"=>"par territoire",
	"b"=>"par region",
	);
  if ($pa=="b") {
	$are["c"]="par projet";
	$are["d"]="par type de projet";
	$are["e"]="par universite";
	$are["f"]="par type d'universite";
	$are["g"]="par fili�re de l'�tudiant";
	$are["h"]="par �tudiant adh�rent";
	$are["i"]="par �tudiant r�f�rent";
	$are["j"]="par niveau d'�tude";
	$are["k"]="par statut de l'�tudiant";
	$are["l"]="par sexe de l'�tudiant";
	$are["m"]="par mode d'arriv�e de l'�tudiant";
  }
  if ($pa=="d") {
	$are["n"]="par classe de l'enfant";
	$are["o"]="par sexe de l'enfant";
	$are["p"]="par �tablissement de l'enfant";
	$are["q"]="par type d'�tablissement";
	$are["t"]="par projet";
	$are["u"]="par type de projet";
  }
  if ($pa=="e") {
	$are["r"]="par type d'�tablissement";
  }
  if ($pa=="f") {
	$are["s"]="par type d'universit�";
  }

?>

Choisissez selon quel crit�re seront regroup�es les donn�es que vous voulez CALCULER : <br />
<select name="rre" onchange="document.forms.st.submit()">
<?php array_sel($rre,$are); ?>
</select>

<br />

<?php
 }









if ($r=="a" || $r=="b" || $r=="c") {
// Choix de l'ordre du tri pour les listes : 

}

?>








<!-- ########################################### -->

<hr />

<?php

// Si on a rempli suffisemment de crit�res, on peut soit 
//   - Acc�der au r�sultat
//   - Stocker cette requete comme une favorite.

if (
    ($pa!="z" && (($q=="a" && $qa) || ($q=="b" && $qb) || $q=="z") && ($r=="a" || $r=="b" || $r=="c" || $r=="d" || ($r=="e" && $rre!="z")) )
    ) {
?>
<p>
Vous avez rempli tous les crit�res, vous pouvez maintenant proc�der au calcul ou sauver cette requ�te pour plus tard
</p>
<p>
<input type="submit" name="compute" value="- = - = - Proc�der au calcul - = - = -" style="margin: 3px" />
<br />
<input type="submit" name="saveit" value="Sauver cette requ�te (entrez un nom ci-contre)" style="margin: 3px" />
 Nom de la requ�te : <input type="text" name="qname" value="<?php echo fl(stripslashes($qname)); ?>" size="30" maxlength="255" /> 
</p>
<?php
    }
?>
<p>
<input type="submit" name="back" value="Annuler, Retour" style="margin: 3px" />
</p>

<?php
include("foot.php");

*/

?>