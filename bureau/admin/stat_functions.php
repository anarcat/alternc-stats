<?php

function stripslashes_rec($vals) {
  if (is_array($vals)) {
    foreach ($vals as $key=>$val) {
      $vals[$key]=stripslashes_rec($val);
    }
  } else {
    $vals = stripslashes($vals);
  }
  return $vals;
}

function addslashes_rec($vals) {
  if (is_array($vals)) {
    foreach ($vals as $key=>$val) {
      $vals[$key]=addslashes_rec($val);
    }
  } else {
    $vals = addslashes($vals);
  }
  return $vals;
}

function array_sel($cur,$arr) {
  reset($arr);
  while(list($k,$v)=each($arr)) {
    echo "<option value=\"$k\"";
    if ($cur==$k) echo " selected=\"selected\"";
    echo ">$v</option>";
  }
}

function array_multiple($cur,$arr) {
  reset($arr);
  while(list($k,$v)=each($arr)) {
    echo "<option value=\"$k\"";
    if (in_array($k,$cur)) echo " selected=\"selected\"";
    echo ">$v</option>";
  }
}

function date_ts2fr($str) {
  if (!$str) return "";
  return substr($str,6,2)."/".substr($str,4,2)."/".substr($str,0,4);
}


?>