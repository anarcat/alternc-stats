<?php
/*
 $Id: stats_show_per_month.php 2493 2009-04-02 14:38:42Z mlutfy $
 ----------------------------------------------------------------------
 AlternC - Web Hosting System
 Copyright (C) 2002 by the AlternC Development Team.
 http://alternc.org/
 ----------------------------------------------------------------------
 Based on:
 Valentin Lacambre's web hosting softwares: http://altern.org/
 ----------------------------------------------------------------------
 LICENSE

 This program is free software; you can redistribute it and/or
 modify it under the terms of the GNU General Public License (GPL)
 as published by the Free Software Foundation; either version 2
 of the License, or (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 To read the license please visit http://www.gnu.org/copyleft/gpl.html
 ----------------------------------------------------------------------
 Original Author of file:
 Purpose of file:
 ----------------------------------------------------------------------
*/
require_once("../class/config.php");

// max 100 months, 12 by default
$count = ($_GET['count'] ? $_GET['count'] % 100: 12);

function quota_get_months($count) {
  global $db;
  $months = array();

  // figure out YYYY-MM for the last N months
  $db->query("SELECT DISTINCT date_format(day, '%Y-%m') AS `month` FROM stat_http
  WHERE `day` < CONCAT( date_format( DATE_ADD( NOW( ) , INTERVAL 1 MONTH ) , '%Y-%m' ) , '-01' )
  AND `day` >= CONCAT( date_format( DATE_SUB( NOW( ) , INTERVAL " . (int) ($count - 1) . " MONTH ) , '%Y-%m' ) , '-01' )
  ORDER BY month ASC");
  
  while ($db->next_record()) {
    $months[] = $db->f('month');
  }

  return $months;
}


function quota_get_usage_by_month($count) {
  global $cuid, $db;

  $usage_all = array();

  // if reseller account, get list of created accounts
  $child_accounts = array($cuid);
  
  if ($cuid != 2000) {
    $db->query("SELECT uid FROM `membres` WHERE creator = $cuid");
  
    while($db->next_record()) {
      $child_accounts[] = $db->f('uid');
    }
  }

  // the stats in the last $count months for all accounts
  $db->query("
  SELECT SUM( s.size ) AS cnt, s.uid, date_format( DAY , '%Y-%m' ) AS `month`, m.login, q.total as bwtotal
  FROM `stat_http` s, `membres` m, `quotas` q
  WHERE q.uid = m.uid AND q.name = 'bw_web'
  AND `day` < CONCAT( date_format( DATE_ADD( NOW( ) , INTERVAL 1 MONTH ) , '%Y-%m' ) , '-01' )
  " . ($cuid != 2000 ? " AND m.uid IN ( " . implode(',', $child_accounts) . ")" : '') . "
  AND `day` >= CONCAT( date_format( DATE_SUB( NOW( ) , INTERVAL " . (int) ($count - 1) . " MONTH ) , '%Y-%m' ) , '-01' ) 
  AND m.uid = s.uid
  GROUP BY `uid` , `month`
  ORDER BY `login`, `month` ASC
  ");

  while ($db->next_record()) {
    $usage_all[$db->f("uid")][$db->f("month")] = $db->f("cnt");
    $usage_all[$db->f("uid")]["login"] = $db->f("login");
    $usage_all[$db->f("uid")]["bwtotal"] = $db->f("bwtotal");
  }

  ksort($usage_all);
  return $usage_all;
}

// $month param is not used for now, only generate stats for current month
function quota_get_usage_details_month($cuid, $month = 0, $domain = '') {
  global $db;

  $usage_month = array();
  $domain = (checkfqdn($domain) == 0 ? $domain : '');

  $q = "SELECT s.size AS cnt, s.uid, s.domain, date_format( DAY , '%Y-%m-%d' ) AS `month`, m.login
         FROM `stat_http` s, `membres` m
        WHERE `day` <=  NOW()
          AND `day` >= date_format( DATE_SUB( NOW( ) , INTERVAL 1 MONTH ) , '%Y-%m-%d' )
          AND m.uid = $cuid
          AND m.uid = s.uid " .
          ( $domain ? " AND s.domain = '$domain' " : '' ) . "
        ORDER BY month ASC, day ASC";

  $db->query($q);
  
  while ($db->next_record()) {
    $usage_month[$db->f('domain')][$db->f('month')] = $db->f('cnt');
    $usage_month['login'] = $db->f('login');
  }

  return $usage_month;
}

if ($_REQUEST['graph']) {
  $overquota = false;
  $domain = $_REQUEST['graphdomain'];

  if (checkfqdn($domain)) {
    $domain = '';
  }

  // c.f. http://pear.veggerby.dk/wiki/image_graph:getting_started_guide
  require 'Image/Graph.php';

  $Graph =& Image_Graph::factory('graph', array(750, 300)); 

  if ($_REQUEST['details'] == 'byday') {
    $usage = quota_get_usage_details_month($cuid, 1, $domain);
    $dataset_domains = array();

    $Graph->add(
      Image_Graph::vertical(
        Image_Graph::factory('title', array('Daily bandwidth usage for ' . $usage['login'], 11)),
        Image_Graph::vertical(
          $Plotarea = Image_Graph::factory('plotarea'),
          $Legend = Image_Graph::factory('legend'),
          90
          ),
        5
        )
    );

    $Plotarea =& $Graph->addNew('plotarea'); 

    // make the legend use the plotarea (or implicitly it's plots)
    $Legend->setPlotarea($Plotarea); 

    foreach ($usage as $key => $val) {
      if ($key != 'login' && $key != 'bwtotal') {
        $dset =& Image_Graph::factory('dataset');

        foreach ($val as $key2 => $val2) {
          $month_label = substr($key2, 5); // remove year
          $dset->addPoint($month_label, $val2 / 1024 / 1024 / 1024);
        }

        $dataset_domains[$key] = $dset;
      }
    }

    if (count($dataset_domains)) {
      $colors = array(0 => 'green', 1 => 'red', 2 => 'blue', 3 => 'yellow', 4 => 'black', 5 => 'brown');
      $color_counter = 0;

      foreach ($dataset_domains as $domain => $data) {
        $Plotx =& $Plotarea->addNew('line', &$data);
        $Plotx->setTitle($domain);
        $Plotx->setLineColor($colors[$color_counter] . '@1.1');
        $color_counter = ($color_counter + 1) % 6;
      }
    }

    $AxisX =& $Plotarea->getAxis(IMAGE_GRAPH_AXIS_X);
    $AxisX->setTitle('Day');
    $AxisY =& $Plotarea->getAxis(IMAGE_GRAPH_AXIS_Y);
    $AxisY->setTitle('Gb', 'vertical'); 
  } else {
    $monthly_size = array();
    $months = quota_get_months($count);
    $usage_all = quota_get_usage_by_month($count);
    $uid = $_REQUEST['uid'];

    $Dataset =& Image_Graph::factory('dataset'); 
    $Dataset2 =& Image_Graph::factory('dataset'); 

    $Dataset_domain = array();

    $Graph->add(
        Image_Graph::vertical(
          Image_Graph::factory('title', array('Monthly bandwidth usage for ' . $usage_all[$uid]['login'], 11)),
          Image_Graph::vertical(
            $Plotarea = Image_Graph::factory('plotarea'),
            $Legend = Image_Graph::factory('legend'),
            90
            ),
          5
          )
         );

    $Plotarea =& $Graph->addNew('plotarea'); 

    if ($usage_all[$uid]) {
      foreach ($usage_all[$uid] as $key => $val) {
        if ($key != 'login' && $key != 'bwtotal') {
          $Dataset->addPoint($key, $val / 1024 / 1024 / 1024); 
          $Dataset2->addPoint($key, $data['bwtotal'] / 1024 / 1024 / 1024);
  
          if ($val > $data['bwtotal']) {
            $overquota = true;
          }
        }
      }
  
  
      $Plot =& $Plotarea->addNew('bar', &$Dataset); 
      $Plot->setFillColor('blue@0.3');
  
      if ($overquota) {
        $Plot2 =& $Plotarea->addNew('line', &$Dataset2); 
        $Plot2->setFillColor('red@0.1');
      }
    }
  }

  $Graph->done();
  exit;
}

include("head.php");

global $cuid;

?>
<style>
.right { text-align: right; }
</style>
</head>
<body>
<h3><?php __("Bandwidth usage in the last $count months"); ?></h3>

<form method="GET">
Months: <input type="text" size="3" name="count" value="<?=$count?>">
<input type="submit" value="<?=_("Go")?>">
</form>
<?php

/*
 * find all the entries for a user
 */
$current_user = null;

// Print header
$class = ($class== 'lst1' ? 'lst2' : 'lst1');
print "<table><tr class=\"$class\"><th class=\"right\">"._("User")."</th>";

$monthly_size = array();
$months = quota_get_months($count);
$usage_all = quota_get_usage_by_month($count);

foreach ($months as $m) {
  print "<th>$m</th>";
}

foreach ($usage_all as $uid => $u) {
  $class = ($class== 'lst1' ? 'lst2' : 'lst1');
  print "<tr class=\"$class\"><td class=\"right\"><acronym title=\"$current_user\">".$u['login']."</acronym></td>\n";

  foreach ($months as $m) {
    print "<td>" . m_quota::display_val('bw_web', $u[$m]) . "</td>\n";
    $monthly_size[$m] += $u[$m];
  }

  // print "<td><a href='stats_show_per_month.php?graph=" . $u['login'] . "&graphdata=" . htmlspecialchars(serialize($u)) . "' target='_new'>G</a></td>";
  print "<td><a href='stats_show_per_month.php?graph=1&uid=" . $uid . "' target='_new'>G</a></td>";
  print "</tr>\n";
}

print "</tr>\n";
print "<tr>\n";
print "<td class=\"right\">"._("Total")."</td>";

foreach ($monthly_size as $m => $size) {
  print "<td>".m_quota::display_val('bw_web', $size)."</td>";
}

print "</table>";

// Show bandwidth usage for the past 12 months
if ($cuid != 2000) {
  print "<img src='stats_show_per_month.php?graph=1&uid=" . $cuid . "' />";
}

// Show bandwidth usage for the past 30 days
print "<img src='stats_show_per_month.php?graph=1&details=byday&graphdomain={$_REQUEST['graphdomain']}' />";
print "<ul>";

$usage = quota_get_usage_details_month($cuid, 1, $domain);

foreach ($usage as $key => $val) {
  if ($key != 'login' && $key != 'bwtotal') {
    $total = 0;
    foreach ($val as $key2 => $val2) {
      $total += $val2;
    }

    print '<li><a href="stats_show_per_month.php?graphdomain=' . $key . '">' . $key . '</a> (' . format_size($total) . ')</li>';
  }
}

print "</ul>";

?>
</body>
</html>
